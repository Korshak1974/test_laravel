<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $username = 'Novak'. rand(1,20);
        return view('home', ['userName' => $username]);
    }
}
